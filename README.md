# recruitment-exercise-1

#### Opis

Zadanie polega na stworzeniu aplikacji składającej się z dwóch części.  
Pierwsza część (backend) to proces Node.js startujący serwer WebSocket.  
Druga część (frontend) to strona internetowa łącząca się z serwerem za pomocą WebSocket.  
Po nawiązaniu połączenia backend ma wczytać z dysku obraz i wysłać go do frontendu jako tablica bajtów, a frontend ma go wyświetlić w przeglądarce.

#### Wymagania

- język: JavaScript/TypeScript  
- biblioteka WebSocket dla Node.js: ws (https://www.npmjs.com/package/ws)